package com.example;

import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {

    @Test
    public void add() {
        assertEquals(3, Calculator.add(1, 2));
        assertEquals(3, Calculator.add(2, 1));
    }

    @Test
    public void stuff() {
	    assertEquals(1, 1);
    }
}
